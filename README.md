Role Nvidia Driver & Cuda
=========

Ansible role to install silently the last nvidia driver & cuda 11

Compatibility
------------

This role can be used only on Ubuntu 18.04. Other OS will come later

Author Information
------------------

Written by [Emmanuel Jaep](mailto:emmanuel.jaep@epfl.ch) for EPFL - STI school of engineering
